LoginGet = function () {
  method = 'post'
  url = api_base_url + 'user/login/get'
  data = JSON.stringify({
    email: $('#email').val(),
    password: md5($('#password').val())
  })
  dataType = 'json'
  contentType = 'application/json'
  type = 'json'
  success = function (response, status, xhr) {
    status = response.status
    error = response.error
    message = response.message
    if (error == true) {
      $('#err_msg').html(exclamation + message)
      return false
    }
    data = response.data
    $.each(data, function (index, value) {
      set_storage('snaptradar_test', JSON.stringify({
        first_name: value.first_name,
        last_name: value.last_name,
        gender: value.gender,
        birthday: value.birthday,
        phone_number: value.phone_number,
        address: value.address,
        city: value.city,
        country: value.country,
        postal_code: value.postal_code,
        auth_key: value.auth_key,
        email: $('#email').val(),
      }))
    })
    window.location = base_url + "dashboard.html"
  }
  error = function (jqXhr, textStatus, errorMessage) {

  }
  connection(method, url, data, type, dataType, contentType, success, error)
  return false
}

SignupAdd = function () {
  method = 'post'
  url = api_base_url + 'user/signup/add'
  data = JSON.stringify({
    first_name: $('#reg-fn').val(),
    last_name: $('#reg-ln').val(),
    email: $('#reg-email').val(),
    phone_number: $('#reg-phone').val(),
    password: md5($('#reg-pass').val())
  })
  dataType = 'json'
  contentType = 'application/json'
  type = 'json'
  success = function (response, status, xhr) {
    status = response.status
    error = response.error
    message = response.message
    if (error == true) {
      $('#err_msg').html(exclamation + message)
      return false
    }
//    window.location = base_url + "index.html"
  }
  error = function (jqXhr, textStatus, errorMessage) {
  }
  connection(method, url, data, type, dataType, contentType, success, error)
  return false
}

ForgetPasswordEdit = function () {
  method = 'post'
  url = api_base_url + 'user/forget_password/edit'
  data = JSON.stringify({
    email: $('#email-for-pass').val()
  })
  dataType = 'json'
  contentType = 'application/json'
  type = 'json'
  success = function (response, status, xhr) {
    //    console.log(response)
    //        forgotpassword.setState({ error_message: <Fragment><p className="information_message"> An email has been sent to your mailbox... </p></Fragment> })
    //        setTimeout(function () { navigate(`/`) }, 5000)
    //        return false
  }
  error = function (jqXhr, textStatus, errorMessage) {
  }
  connection(method, url, data, type, dataType, contentType, success, error)
  return false
}

SalesGet = function () {
    storage = get_storage('snaptradar_test')

    /*{"first_name":"paul","last_name":".","gender":"M","birthday":"1982-05-06","phone_number":"0815971903311","address":"pekapuran11","city":"jakarta11","country":"indonesia11","postal_code":"1121011","auth_key":"8a2e52f231d2d7a3cd06903d20953f509f07a2945a8f9fa0658cab901e8d96ea0fb020c144406c1c74f2e701ea0aef4f10dc2eb6ce6bde7b2447993c4d5dd37b"} */
    sp = JSON.parse(storage)
    url = api_base_url + 'user/product/get'

    data = JSON.stringify({
        auth_key: sp.auth_key,
    })
    dataType = 'json'
    contentType = 'application/json'
    type = 'json'
    success = function (response, status, xhr) {
        status = response.status
        error = response.error
        message = response.message
        if (error == true) {
            return false
        }
        data = response.data
        res = ''
        $.each(data, function (index, value) {
          res += '<tr>'+
              '<td>'+(index+1)+'</td>'+
              '<td>'+value.product_name+'</td>'+
              '<td>'+value.product_quantity+'</td>'+
              '<td>'+value.created_date+'</td>'+
              '<tr>'
        })
        console.log(data)
        $('#sales tbody ').html(res)
    }
    error = function (jqXhr, textStatus, errorMessage) {
    }
    connection(method, url, data, type, dataType, contentType, success, error)
    return false

}

ProfileGet = function () {
  storage = get_storage('snaptradar_test')

  /*{"first_name":"paul","last_name":".","gender":"M","birthday":"1982-05-06","phone_number":"0815971903311","address":"pekapuran11","city":"jakarta11","country":"indonesia11","postal_code":"1121011","auth_key":"8a2e52f231d2d7a3cd06903d20953f509f07a2945a8f9fa0658cab901e8d96ea0fb020c144406c1c74f2e701ea0aef4f10dc2eb6ce6bde7b2447993c4d5dd37b"} */
  sp = JSON.parse(storage)
  var date = new Date(sp.birthday)
  month = ("0" + (date.getMonth() + 1)).slice(-2)
  day = ("0" + date.getDate()).slice(-2)
  year = date.getFullYear()

  $('#account-fn').val(sp.first_name)
  $('#account-ln').val(sp.last_name)
  $('#account-email').val(sp.email)
  $('#account-dob').val(month + '/' + day + '/' + year)
  $('#account-gender').val(sp.gender)
  $('#account-phone').val(sp.phone_number)
  return false
}

ProductAdd = function () {
    storage = get_storage('snaptradar_test')
    sp = JSON.parse(storage)
    method = 'post'
    url = api_base_url + 'user/product/add'

    data = JSON.stringify({
        auth_key: sp.auth_key,
        product_name: $('#product-nm').val(),
        product_qty: $('#product-qty').val(),
    })
    dataType = 'json'
    contentType = 'application/json'
    type = 'json'
    success = function (response, status, xhr) {
        iziToast.success({
            title: 'Success!',
            message: 'Your product added',
            position: 'topRight',
            progressBar: true,
            icon: "icon-circle-check",
        })
    }
    error = function (jqXhr, textStatus, errorMessage) {
    }
    connection(method, url, data, type, dataType, contentType, success, error)
    return false

}

ProfileEdit = function () {
  storage = get_storage('snaptradar_test')
  /*{"first_name":"paul","last_name":".","gender":"M","birthday":"1982-05-06","phone_number":"0815971903311","address":"pekapuran11","city":"jakarta11","country":"indonesia11","postal_code":"1121011","auth_key":"8a2e52f231d2d7a3cd06903d20953f509f07a2945a8f9fa0658cab901e8d96ea0fb020c144406c1c74f2e701ea0aef4f10dc2eb6ce6bde7b2447993c4d5dd37b"} */
  sp = JSON.parse(storage)
  method = 'post'
  url = api_base_url + 'user/profile/edit'

  data = JSON.stringify({
    auth_key: sp.auth_key,
    first_name: $('#account-fn').val(),
    last_name: $('#account-ln').val(),
    birthday: $('#account-dob').val().replace(/(\d\d)\/(\d\d)\/(\d{4})/, "$3-$1-$2"),
    address: '',
    city: '',
    country: '',
    postal_code: '',
    gender: $('#account-gender').val(),
    phone_number: $('#account-phone').val(),
    password: md5($('#account-pass').val()),
  })
  dataType = 'json'
  contentType = 'application/json'
  type = 'json'
  success = function (response, status, xhr) {
    iziToast.success({
      title: 'Success!',
      message: 'Your profile updated successfully',
      position: 'topRight',
      progressBar: true,
      icon: "icon-circle-check",
    })
  }
  error = function (jqXhr, textStatus, errorMessage) {
  }
  connection(method, url, data, type, dataType, contentType, success, error)
  return false
}

