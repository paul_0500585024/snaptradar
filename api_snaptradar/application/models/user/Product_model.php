<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {
		
	public function get($data){
		$this->db->select('id, product_name, product_quantity, created_date');
		$this->db->where('user_id', $data['user_id']);
		return $this->db->get('m_sales');
	}

    public function add($data){
        $this->db->insert('m_sales',$data);
    }


}