<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	
	public function get($data){
		$this->db->select('id, first_name, last_name, gender, birthday, phone_number, address, city, country, postal_code, ip_addr, last_login');
		$this->db->where('email', $data['email']);
		$this->db->where('password', $data['password']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}

	public function get_by_auth_key($data){
		$this->db->select('id, email, first_name, last_name, gender, birthday, phone_number, address, city, country, postal_code, ip_addr, last_login');
		$this->db->where('auth_key', $data['auth_key']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
		
	public function edit($id, $auth_key){
		$params = new stdClass();
		$params->auth_key = $auth_key;
		$params->last_login = date('Y-m-d H:i:s');
		$this->db->update('m_user',$params,array('id' => $id));
	}
	
}