<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Check_session_model extends CI_Model {
	
	public function get($data){
		$this->db->select('id, email, first_name, last_name, gender, birthday, phone_number, address, city, country, postal_code, ip_addr, last_login');
		$this->db->where('auth_key', $data['auth_key']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
			
}