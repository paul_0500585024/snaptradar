<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model {
		
	public function get($data){
		$this->db->select('id, first_name, last_name, email, phone_number, address, city, country, postal_code, ip_addr, last_login');
		$this->db->where('auth_key', $data['auth_key']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
	
	public function edit($data){
		$params = new stdClass();
		$params->first_name = $data['first_name'];
		$params->last_name = $data['last_name'];
		$params->password = $data['password'];
		$params->phone_number = $data['phone_number'];
		$params->address = $data['address'];
		$params->city = $data['city'];
		$params->country = $data['country'];
		$params->postal_code = $data['postal_code'];
		$params->birthday = $data['birthday'];
		$params->gender = $data['gender'];
		$this->db->update('m_user',$params,array('auth_key' => $data['auth_key']));	
	}

	
}