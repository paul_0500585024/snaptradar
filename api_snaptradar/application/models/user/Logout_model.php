<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout_model extends CI_Model {
	
	public function edit($data){
		$params = new stdClass();
		$params->auth_key = '';
		$this->db->update('m_user',$params,array('auth_key' => $data['auth_key']));
	}
	
}