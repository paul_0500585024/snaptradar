<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Validate_new_password_model extends CI_Model {

	public function get($data){
		$this->db->select('id, first_name, last_name, email, ip_addr, last_login');
		$this->db->where('auth_key', $data['auth_key']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
	
	public function edit($data){
		$params = new stdClass();
		$params->password = $data['password'];
		$this->db->update('m_user',$params,array('auth_key' => $data['auth_key']));		
	}
	
}