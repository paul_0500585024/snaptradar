<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Signup_model extends CI_Model {
	
	public function add($data){
		$data['active'] = 1;
		$this->db->insert('m_user',$data);
	}
	
	public function get($data){
		$this->db->select('id, first_name, last_name, gender, birthday, phone_number, address, city, country, postal_code, ip_addr, last_login');
		$this->db->where('email', $data['email']);
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
		
}