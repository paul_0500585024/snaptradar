<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userdb extends CI_Model {
	
	public function select_user($data){
		$this->db->select('id, name, ip_addr, last_login');
		$this->db->where('email', $data['email']);
		$this->db->where('password', $data['password']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
	
	public function select_user_by_auth_key($data){
		$this->db->select('id, name, email, ip_addr, last_login');
		$this->db->where('auth_key', $data['auth_key']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
	
	public function update_user_auth_key($id, $auth_key){
		$params = new stdClass();
		$params->auth_key = $auth_key;
		$this->db->update('m_user',$params,array('id' => $id));
	}

	public function update_user_auth_key_by_email($email, $auth_key){
		$params = new stdClass();
		$params->auth_key = $auth_key;
		$this->db->update('m_user',$params,array('email' => $email));
	}

	public function update_user_auth_key_clear($data){
		$params = new stdClass();
		$params->auth_key = '';
		$this->db->update('m_user',$params,array('auth_key' => $data['auth_key']));
	}

	public function add_user($data){
		$data['active'] = 1;
		$this->db->insert('m_user',$data);
	}
	
	public function select_user_auth_key($data){
		$this->db->select('id, name, email, ip_addr, last_login');
		$this->db->where('auth_key', $data['auth_key']);
		$this->db->where('active', '1');
		$this->db->limit(1);
		return $this->db->get('m_user');
	}
	
	public function update_user_password_by_auth_key($data){
		$params = new stdClass();
		$params->password = $data['password'];
		$this->db->update('m_user',$params,array('auth_key' => $data['auth_key']));		
	}
	
	public function update_user_name_password_by_auth_key($data){
		$params = new stdClass();
		$params->name = $data['name'];
		$params->password = $data['password'];
		$this->db->update('m_user',$params,array('auth_key' => $data['auth_key']));				
	}
	
}