<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

require_once APPPATH.'libraries/random_compat/lib/random.php';

class Forget_password extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user/Forget_password_model');
		$this->load->library('email');
        $this->config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => '465',
            'smtp_user' => 'test.paul82@gmail.com',
            'smtp_pass' => 'MCCNmh4wNJqararq',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );		
	}
	
	private function random_code($limit)
	{
		return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
	}
	
	public function edit(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$auth_key = bin2hex(random_bytes(64));
			$params['email'] = $decoder->email;
			
			$password = $this->random_code(8);
			
//			$this->Forget_password_model->edit($params['email'],$auth_key);
			$this->Forget_password_model->edit($params['email'],md5($password));

			$this->email->initialize($this->config);
			$this->email->from('test.paul82@gmail.com', 'paul');
			$this->email->to($params['email']);
			$this->email->subject('Forget Password');
//			$mail_message = 'Please click this link : '.WEB_URL.'validatenewpassword?auth_key='. $auth_key;
			$mail_message = 'Use the code to change your password on our secure website : '.$password;
			$this->email->message($mail_message);

			$message = 'Email not sent';
			if (!$this->email->send()) {
			}else{
				$message = 'Email sent';
			}				

			$response['status']= 200;
			$response['error']= false;
			$response['message'] = $message;
			echo json_encode($response);
		}		
	}
	
}
