<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

class Validate_new_password extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user/Validate_new_password_model');		
	}
	
	public function get() {
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			
			$query = $this->Validate_new_password_model->get($params);
			
			$response['status']=200;
			$response['error']=false;
			$response['message'] = false;
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					//only 1 query exisst
					$query_data['id'] = $each->id;
					$query_data['first_name'] = $each->first_name;
					$query_data['last_name'] = $each->last_name;
					$query_data['auth_key'] = $params['auth_key'];
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
			}else{
				$response['data'] = array();
			}		
			echo json_encode($response);
		}		
		
	}
		
	public function edit() {
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			$params['password'] = $decoder->new_password;
			$this->Validate_new_password_model->edit($params);
			$response['status']= 200;
			$response['error']= false;
			$response['message'] = 'Password Updated';
			echo json_encode($response);
		}		
	}

}
