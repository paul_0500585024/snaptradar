<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

class Profile extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user/Profile_model');
		$this->load->library('email');
        $this->config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => '465',
            'smtp_user' => 'test.paul82@gmail.com',
            'smtp_pass' => 'MCCNmh4wNJqararq',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );
		
	}
	
	public function get() {
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			
			$query = $this->Profile_model->get($params);
			$response['status']=200;
			$response['error']=false;
			$response['message'] = false;
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					//only 1 query exisst
					$query_data['id'] = $each->id;
					$query_data['first_name'] = $each->first_name;
					$query_data['last_name'] = $each->last_name;
					$query_data['email'] = $each->email;
					$query_data['auth_key'] = $params['auth_key'];
					$query_data['phone_number'] = $each->phone_number;
					$query_data['address'] = $each->address;
					$query_data['city'] = $each->city;
					$query_data['country'] = $each->country;
					$query_data['postal_code'] = $each->postal_code;
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
			}else{
				$response['data'] = array();
			}		
			echo json_encode($response);
		}		
	}
	
	public function edit() {
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			$params['birthday'] = $decoder->birthday;
			$params['gender'] = $decoder->gender;
			$params['first_name'] = $decoder->first_name;			
			$params['last_name'] = $decoder->last_name;			
			$params['password'] = $decoder->password;
			$params['phone_number'] = $decoder->phone_number;
			$params['address'] = $decoder->address;
			$params['city'] = $decoder->city;
			$params['country'] = $decoder->country;
			$params['postal_code'] = $decoder->postal_code;
			$this->Profile_model->edit($params);
			$response['status']= 200;
			$response['error']= false;
			$response['message'] = 'Profile Updated';
			echo json_encode($response);
		}		
	}	
}
