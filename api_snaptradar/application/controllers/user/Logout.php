<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

class Logout extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user/Logout_model');		
	}
	
	public function edit(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;			
			$this->Logout_model->edit($params);
			$response['status']= 200;
			$response['error']= false;
			$response['message'] = 'auth key cleared';
			echo json_encode($response);
		}
	}
	
}
