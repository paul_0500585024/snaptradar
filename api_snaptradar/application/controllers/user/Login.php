<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

require_once APPPATH.'libraries/random_compat/lib/random.php';

class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('user/Login_model');		
	}
	
	public function get(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['email'] = $decoder->email;
			$params['password'] = $decoder->password;
            $auth_key = bin2hex(random_bytes(64));
//          $token = bin2hex(random_bytes(64));
			
//			$params['email'] = 'paul_0500585024@yahoo.com';
//			$params['password'] = '25d55ad283aa400af464c76d713c07ad';
			
			$query = $this->Login_model->get($params);
			$response['status']=200;
			$response['error']=false;
			$response['message'] = false;
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					//only 1 query exist
					$query = $this->Login_model->edit($each->id,$auth_key);	 //update auth_key
					$query_data['first_name'] = $each->first_name;
					$query_data['last_name'] = $each->last_name;
					$query_data['gender'] = $each->gender;
					$query_data['birthday'] = $each->birthday;
					$query_data['phone_number'] = $each->phone_number;
					$query_data['address'] = $each->address;
					$query_data['city'] = $each->city;
					$query_data['country'] = $each->country;
					$query_data['postal_code'] = $each->postal_code;					
					$query_data['auth_key'] = $auth_key;
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
			}else{
				$response['data'] = array();
				$response['error'] = true;
				$response['message'] = 'Incorrect user name or password';
			}		
			echo json_encode($response);
		}
	}
	
	public function get_by_auth_key(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
            $auth_key = bin2hex(random_bytes(64));
//          $token = bin2hex(random_bytes(64));
			
//			$params['email'] = 'paul_0500585024@yahoo.com';
//			$params['password'] = '25d55ad283aa400af464c76d713c07ad';
			
			$query = $this->Login_model->get_by_auth_key($params);
			$response['status']=200;
			$response['error']=false;
			$response['message'] = false;
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					//only 1 query exist
					$query = $this->Login_model->edit($each->id,$auth_key);	 //update auth_key
					$query_data['first_name'] = $each->first_name;
					$query_data['last_name'] = $each->last_name;
					$query_data['gender'] = $each->gender;
					$query_data['birthday'] = $each->birthday;
					$query_data['phone_number'] = $each->phone_number;
					$query_data['address'] = $each->address;
					$query_data['city'] = $each->city;
					$query_data['country'] = $each->country;
					$query_data['postal_code'] = $each->postal_code;					
					$query_data['auth_key'] = $auth_key;
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
				$date_diff = strtotime("now") - strtotime($data[0]['last_login']);
				if($date_diff > (60 * 60 * 24)){ //session given for 1 day
//				if($date_diff > 0){
					$response['data'] = array();
					$response['error'] = true;
					$response['message'] = 'Session Expired';
				}				
			}else{
				$response['data'] = array();
				$response['error'] = true;
				$response['message'] = 'Incorrect auth key';
			}		
			echo json_encode($response);
		}		
	}
}
