<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

class Check_session extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user/Check_session_model');		
	}
	
	public function get(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			$query = $this->Check_session_model->get($params);
			$response['status']=200;
			$response['error']='false';
			$response['message'] = 'false';
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					$query_data['email'] = $each->email;
					$query_data['first_name'] = $each->first_name;
					$query_data['last_name'] = $each->last_name;
					$query_data['gender'] = $each->gender;
					$query_data['birthday'] = $each->birthday;
					$query_data['phone_number'] = $each->phone_number;
					$query_data['address'] = $each->address;
					$query_data['city'] = $each->city;
					$query_data['country'] = $each->country;
					$query_data['postal_code'] = $each->postal_code;					
					$query_data['auth_key'] = $params['auth_key'];
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
				$date_diff = strtotime("now") - strtotime($data[0]['last_login']);
				if($date_diff > (60 * 60 * 24)){ //session given for 1 day
//				if($date_diff > 0){
					$response['data'] = array();
					$response['error'] = true;
					$response['message'] = 'Session Expired';
				}
			}else{
				$response['data'] = array();
				$response['error'] = true;
				$response['message'] = 'Incorrect user name or password';
			}		
			echo json_encode($response);						
		}
	}
	
}
