<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

class Product extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user/Profile_model');
        $this->load->model('user/Product_model');

	}

	public function get() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $param = file_get_contents("php://input");
            $decoder = json_decode($param);
            $params['auth_key'] = $decoder->auth_key;

            $query = $this->Profile_model->get($params);
            if($query->num_rows() > 0) {
                $data = array();
                //get user_id
                foreach ($query->result() as $each) {
                    //only 1 query exisst
                    $query_data_profile['id'] = $each->id;
                    $query_data_profile['user_id'] = $each->id;
                    $query_data_profile['first_name'] = $each->first_name;
                    $query_data_profile['last_name'] = $each->last_name;
                    $query_data_profile['email'] = $each->email;
                    $query_data_profile['auth_key'] = $params['auth_key'];
                    $query_data_profile['phone_number'] = $each->phone_number;
                    $query_data_profile['address'] = $each->address;
                    $query_data_profile['city'] = $each->city;
                    $query_data_profile['country'] = $each->country;
                    $query_data_profile['postal_code'] = $each->postal_code;
                    $query_data_profile['ip_addr'] = $each->ip_addr;
                    $query_data_profile['last_login'] = $each->last_login;
                }

                $query_product = $this->Product_model->get($query_data_profile);
                $response['status']=200;
                $response['error']=false;
                $response['message'] = false;
                if($query_product->num_rows() > 0){
                    $data = array();
                    foreach($query_product->result() as $each){
                        //only 1 query exist
                        $query_data['id'] = $each->id;
                        $query_data['product_name'] = $each->product_name;
                        $query_data['product_quantity'] = $each->product_quantity;
                        $query_data['created_date'] = $each->created_date;
                        $data[] = $query_data;
                    }
                    $response['data'] = $data;
                }else{
                    $response['data'] = array();
                    $response['error'] = false;
                    $response['message'] = 'Product not available';
                }
                echo json_encode($response);


            }else{
                $response['status']= 200;
                $response['error']= false;
                $response['message'] = 'Product Show error';
            }
        }

    }

	public function add() {
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
            $params['product_name'] = $decoder->product_name;
            $params['product_qty'] = $decoder->product_qty;

			$query = $this->Profile_model->get($params);
			if($query->num_rows() > 0){
				$data = array();
				//get user_id
				foreach($query->result() as $each){
					//only 1 query exisst
					$query_data['id'] = $each->id;
					$query_data['first_name'] = $each->first_name;
					$query_data['last_name'] = $each->last_name;
					$query_data['email'] = $each->email;
					$query_data['auth_key'] = $params['auth_key'];
					$query_data['phone_number'] = $each->phone_number;
					$query_data['address'] = $each->address;
					$query_data['city'] = $each->city;
					$query_data['country'] = $each->country;
					$query_data['postal_code'] = $each->postal_code;
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
				}

				//add to m_sales
                $params_add['user_id'] = $query_data['id'];
                $params_add['product_name'] = $params['product_name'];
                $params_add['product_quantity'] = $params['product_qty'];
                $params_add['created_by'] = $query_data['id'];
                $params_add['created_date'] = date('Y-m-d H:i:s');
                $params_add['modified_by'] = $query_data['id'];
                $params_add['modified_date'] = date('Y-m-d H:i:s');
                $this->Product_model->add($params_add);

                $response['status']= 200;
                $response['error']= false;
                $response['message'] = 'Product Added success';

			}else{
                $response['status']= 200;
                $response['error']= false;
                $response['message'] = 'Product Added failed';
			}		
		}
	}
	
}
