<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

class Signup extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user/Signup_model');
		$this->load->library('email');
        $this->config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => '465',
            'smtp_user' => 'test.paul82@gmail.com',
            'smtp_pass' => 'MCCNmh4wNJqararq',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );		
	}
	
	public function add(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['first_name'] = $decoder->first_name;
			$params['last_name'] = $decoder->last_name;
			$params['email'] = $decoder->email;
			$params['phone_number'] = $decoder->phone_number;			
			$params['password'] = $decoder->password;
			
			$query = $this->Signup_model->get($params);
			if($query->num_rows() == 0){				
				$this->Signup_model->add($params);
				
				$this->email->initialize($this->config);
				$this->email->from('test.paul82@gmail.com', 'paul');
				$this->email->to($params['email']);
				$this->email->subject('Register');
				$mail_message = 'Thanks for register';
				$this->email->message($mail_message);

				$message = 'Email not sent';
				if (!$this->email->send()) {
				}else{
					$message = 'Email sent';
				}				
				
				$response['status']= 200;
				$response['error']= false;
				$response['message'] = 'Registration success';
			}else{
				$response['status']= 200;
				$response['error']= true;
				$response['message'] = 'Email has already been registered';				
			}			
			echo json_encode($response);
		}
	}
	
}
