<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Request-Method: *");
header("Access-Control-Request-Headers: *");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
//header("Accept: application/json");
//header("Content-type: application/json");

require_once APPPATH.'libraries/random_compat/lib/random.php';

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('Userdb');
		$this->load->library('email');
        $this->config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.mayanfx.com',
            'smtp_port' => '587',
            'smtp_user' => 'test.paul82@gmail.com',
            'smtp_pass' => 'MCCNmh4wNJqararq',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );
		
	}
	
	public function user_get(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['email'] = $decoder->email;
			$params['password'] = $decoder->password;
            $auth_key = bin2hex(random_bytes(64));
//            $token = bin2hex(random_bytes(64));
			
	//		$params['email'] = 'paul_0500585024@yahoo.com';
	//		$params['password'] = '25d55ad283aa400af464c76d713c07ad';
			
			$query = $this->Userdb->select_user($params);
			$response['status']=200;
			$response['error']=false;
			$response['message'] = false;
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					//only 1 query exisst
					$query = $this->Userdb->update_user_auth_key($each->id,$auth_key);					
					$query_data['id'] = $each->id;
					$query_data['name'] = $each->name;
					$query_data['auth_key'] = $auth_key;
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
			}else{
				$response['data'] = array();
			}		
			echo json_encode($response);
		}
	}
	
	public function user_getbyauthkey() {
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			
			$query = $this->Userdb->select_user_by_auth_key($params);
			$response['status']=200;
			$response['error']=false;
			$response['message'] = false;
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					//only 1 query exisst
					$query_data['id'] = $each->id;
					$query_data['name'] = $each->name;
					$query_data['email'] = $each->email;
					$query_data['auth_key'] = $params['auth_key'];
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
			}else{
				$response['data'] = array();
			}		
			echo json_encode($response);
		}		
	}
	
	public function user_auth_clear(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;			
			$this->Userdb->update_user_auth_key_clear($params);
			$response['status']= 200;
			$response['error']= false;
			$response['message'] = 'auth key cleared';
			echo json_encode($response);
		}
	}
	
	public function user_signup(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['name'] = $decoder->name;
			$params['email'] = $decoder->email;
			$params['password'] = $decoder->password;
			$this->Userdb->add_user($params);
			
			$this->email->initialize($this->config);
			$this->email->from('test.paul82@gmail.com', 'paul');
			$this->email->to($params['email']);
			$this->email->subject('Register');
			$mail_message = 'Thanks for register';
			$this->email->message($mail_message);

			$message = 'Email not sent';
			if (!$this->email->send()) {
			}else{
				$message = 'Email sent';
			}				
			
			$response['status']= 200;
			$response['error']= false;
			$response['message'] = 'Registration success';
			echo json_encode($response);
		}
	}
	
	public function user_forgotpassword(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$auth_key = bin2hex(random_bytes(64));
			$params['email'] = $decoder->email;
			
			$this->Userdb->update_user_auth_key_by_email($params['email'],$auth_key);

			$this->email->initialize($this->config);
			$this->email->from('test.paul82@gmail.com', 'paul');
			$this->email->to($params['email']);
			$this->email->subject('Forgot Password');
			$mail_message = 'Please click this link : '.WEB_URL.'validatenewpassword?auth_key='. $auth_key;
			$this->email->message($mail_message);

			$message = 'Email not sent';
			if (!$this->email->send()) {
			}else{
				$message = 'Email sent';
			}				

			$response['status']= 200;
			$response['error']= false;
			$response['message'] = $message;
			echo json_encode($response);
		}		
	}
	
	public function user_validatenewpassword(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			
			$query = $this->Userdb->select_user_auth_key($params);
			
			$response['status']=200;
			$response['error']=false;
			$response['message'] = false;
			if($query->num_rows() > 0){
				$data = array();
				foreach($query->result() as $each){
					//only 1 query exisst
					$query_data['id'] = $each->id;
					$query_data['name'] = $each->name;
					$query_data['auth_key'] = $params['auth_key'];
					$query_data['ip_addr'] = $each->ip_addr;
					$query_data['last_login'] = $each->last_login;	
					$data[] = $query_data;
				}
				$response['data'] = $data;
			}else{
				$response['data'] = array();
			}		
			echo json_encode($response);
			

/*			$this->email->initialize($this->config);
			$this->email->from('test.paul82@gmail.com', 'paul');
			$this->email->to($params['email']);
			$this->email->subject('Forgot Password');
			$mail_message = 'Please click this link : '.WEB_URL.'validatenewpassword?auth_key='. $auth_key;
			$this->email->message($mail_message);

			$message = 'Email not sent';
			if (!$this->email->send()) {
			}else{
				$message = 'Email sent';
			}				

			$response['status']= 200;
			$response['error']= false;
			$response['message'] = $message;
			echo json_encode($response);*/
		}		
	}
	
	public function user_validatesubmitnewpassword(){
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			$params['password'] = $decoder->new_password;
			$this->Userdb->update_user_password_by_auth_key($params);
			$response['status']= 200;
			$response['error']= false;
			$response['message'] = 'Password Updated';
			echo json_encode($response);
		}
	}
	
	public function user_submitbyauthkey() {
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$param = file_get_contents("php://input");
			$decoder = json_decode($param);
			$params['auth_key'] = $decoder->auth_key;
			$params['name'] = $decoder->name;			
			$params['password'] = $decoder->password;
			$this->Userdb->update_user_name_password_by_auth_key($params);
			$response['status']= 200;
			$response['error']= false;
			$response['message'] = 'Profile Updated';
			echo json_encode($response);
		}		
	}
	
	public function index()
	{
		$this->load->view('welcome_message');
	}
}
