/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.28-MariaDB : Database - snaptradar
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`snaptradar` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `snaptradar`;

/*Table structure for table `m_sales` */

DROP TABLE IF EXISTS `m_sales`;

CREATE TABLE `m_sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `m_sales` */

insert  into `m_sales`(`id`,`user_id`,`product_name`,`product_quantity`,`created_by`,`created_date`,`modified_by`,`modified_date`) values (1,1,'product',1,'1','2019-07-18 00:50:10','1','2019-07-18 00:50:10'),(2,1,'product2',2,'1','2019-07-18 00:52:00','1','2019-07-18 00:52:00'),(3,1,'product2',4,'1','2019-07-18 00:58:59','1','2019-07-18 00:58:59');

/*Table structure for table `m_user` */

DROP TABLE IF EXISTS `m_user`;

CREATE TABLE `m_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` enum('M','F') DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `active` enum('1','0') DEFAULT NULL,
  `ip_addr` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `m_user` */

insert  into `m_user`(`id`,`email`,`password`,`first_name`,`last_name`,`gender`,`birthday`,`phone_number`,`address`,`city`,`country`,`postal_code`,`active`,`ip_addr`,`last_login`,`auth_key`,`created_by`,`created_date`,`modified_by`,`modified_date`) values (1,'paul_0500585024@yahoo.com','25d55ad283aa400af464c76d713c07ad','paul chen','paul chen','M','1983-03-17','08159719033112','','','','','1','','2019-07-18 01:02:57','','','0000-00-00 00:00:00','paul','0000-00-00 00:00:00'),(5,'fondation1@gmail.com','25d55ad283aa400af464c76d713c07ad','akong',NULL,NULL,NULL,'12345678',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,'','0000-00-00 00:00:00','','0000-00-00 00:00:00'),(6,'paul.chen@gmail.com','25d55ad283aa400af464c76d713c07ad','paul','chen',NULL,NULL,'08159719033',NULL,NULL,NULL,NULL,'1',NULL,'2019-07-04 06:08:54','728111386dee0deb8bd64e62aebed3f6b0877810c83216495e55104326ecc821e9da0c0d9d36a9546eb4a5d1704ba9c0969dde01a7ddaf5371da74790542e311','','0000-00-00 00:00:00','','0000-00-00 00:00:00'),(16,'paul.evercoss@gmail.com','fbf2b7c5bcb8b6a6758453f2abab4f38','paul','chen',NULL,NULL,'12345678',NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,'','0000-00-00 00:00:00','','0000-00-00 00:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
